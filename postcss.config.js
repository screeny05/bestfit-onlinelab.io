module.exports = (ctx) => ({
    map: {
        inline: false
    },
    plugins: {
        'postcss-flexbugs-fixes': {},
        autoprefixer: {
            browsers: '> 0.5%, last 2 versions'
        },
        cssnano: {
            discardUnused: false
        }
    }
});
