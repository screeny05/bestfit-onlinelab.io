const fs = require('fs');
const path = require('path');

const WebpackNotifierPlugin = require('webpack-notifier');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const packageInfo = require('./package.json');
const hexoConfig = require('js-yaml').safeLoad(fs.readFileSync('./_config.yml', 'utf8'));

const THEME_PATH = path.join(__dirname, 'themes', hexoConfig.theme, 'source');
const THEME_PATH_SRC_JS = path.join(THEME_PATH, '_js');

const extractCss = new ExtractTextPlugin('../css/[name].css');
const extractCssStack = extractCss.extract({
    loader: [{
        loader: 'css-loader',
        query: {
            sourceMap: true
        }
    }, {
        loader: 'postcss-loader'
    }, {
        loader: 'sass-loader',
        query: {
            includePaths: [path.resolve(__dirname, './node_modules')],
            sourceMap: true
        }
    }]
});

module.exports = {
    entry: {
        index: THEME_PATH_SRC_JS,
        'experiments.raytracer': path.join(THEME_PATH_SRC_JS, 'experiments/raytracer/index.js'),
        'experiments.css-parser': path.join(THEME_PATH_SRC_JS, 'experiments/css-parser/index.js'),
        'experiments.synthwave': path.join(THEME_PATH_SRC_JS, 'experiments/synthwave/index.js'),
    },
    output: {
        path: path.join(THEME_PATH, 'js'),
        filename: '[name].js'
    },

    devtool: 'source-map',

    module: {
        rules: [{
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: [{
                loader: 'babel-loader',
            }]
        }, {
            test: /\.vue$/,
            use: [{
                loader: 'vue-loader',
                options: {
                    loaders: {
                        scss: extractCssStack
                    }
                }
            }]
        }, {
            test: /\.json$/,
            use: [{
                loader: 'json-loader'
            }]
        }, {
            test: /\.(css|scss)$/,
            loader: extractCssStack
        }, {
            test: /\.(vs|fs)$/,
            loader: 'raw-loader'
        }],
    },

    plugins: [
        new WebpackNotifierPlugin(),
        extractCss
    ]
};
