import * as THREE from 'three';
window.THREE = THREE;

require('three.terrain.js');
require('./three/mtl-loader');
require('./three/obj-loader');
require('./three/collada-loader');

import EffectComposerConstructor from 'three-effectcomposer';
import Stats from 'stats.js';

import PyramidObject from './objects/pyramid';
import FloorObject from './objects/floor';
import SkydomeObject from './objects/skydome';
import LightObject from './objects/light';
import ControlsObject from './objects/controls';
import CarObject from './objects/car';

import BadTvShader from './shader/bad-tv';

const EffectComposer = EffectComposerConstructor(THREE);
const RenderPass = EffectComposer.RenderPass;
const ShaderPass = EffectComposer.ShaderPass;

class Game {
    scene = null;
    camera = null;
    renderer = null;
    clock = null;
    stats = null;
    running = false;
    objects = [];
    container = null;
    composer = null;
    postProcessingPass = null;

    constructor(container = document.body, fov = 75, near = 0.01, far = 10000){
        const width = window.innerWidth;
        const height = window.innerHeight;

        this.container = container;
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(fov, width / height, near, far);
        this.stats = new Stats();
        this.clock = new THREE.Clock();

        this.renderer = new THREE.WebGLRenderer({ antialias: true });
        this.renderer.setSize(width, height);
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;

        this.composer = new EffectComposer(this.renderer);

        this.container.innerHTML = '';
        this.container.appendChild(this.renderer.domElement);
        this.container.appendChild(this.stats.dom);

        this.scene.fog = new THREE.FogExp2(0xe54aaf, 0.0003);

        this.addShader();
        this.registerEvents();
    }

    addShader(){
        const renderPass = new RenderPass(this.scene, this.camera);
        const badTvPass = new ShaderPass(BadTvShader);
        this.composer.addPass(renderPass);
        this.composer.addPass(badTvPass);
        badTvPass.renderToScreen = true;
        badTvPass.uniforms.rollSpeed.value = 0;
        this.postProcessingPass = badTvPass;
    }

    registerEvents(){
        window.addEventListener('resize', ::this.onWindowResize, false);
    }

    onWindowResize(){
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(window.innerWidth, window.innerHeight);
    }

    start(){
        this.running = true;
        this.stats.showPanel(0);
        this.clock.start();
        this.update();
    }

    stop(){
        this.running = false;
        this.clock.stop();
    }

    update(){
        if(!this.running){
            return;
        }

        const delta = this.clock.getDelta();

        this.objects.forEach(obj => obj.update(delta));

        this.postProcessingPass.uniforms.time.value += delta;

        this.stats.begin();
        this.composer.render();
        this.stats.end();

        window.requestAnimationFrame(this.update.bind(this));
    }

    addObject(obj){
        this.objects.push(obj);
        obj.onAdd(this);
    }
}











const synthwave = new Game();
synthwave.start();
synthwave.addObject(new SkydomeObject());
synthwave.addObject(new PyramidObject());
synthwave.addObject(new FloorObject());
synthwave.addObject(new ControlsObject());
synthwave.addObject(new LightObject());
synthwave.addObject(new CarObject());

console.log(synthwave);
