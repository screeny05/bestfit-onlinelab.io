import GameObject from './base';
import * as THREE from 'three';

export default class Car extends GameObject {
    model = null;
    path = '/models/';
    objPath = 'car.obj';
    mtlPath = 'car.mtl';
    colladaPath = 'model.dae';

    constructor(){
        super();
        this.initModel();
    }

    initModel(){
        const objLoader = new THREE.OBJLoader();
        const mtlLoader = new THREE.MTLLoader();
        const colladaLoader = new THREE.ColladaLoader();

        mtlLoader.setPath(this.path);
        mtlLoader.load(this.mtlPath, materials => {
            materials.preload();

            objLoader.setPath(this.path);
            objLoader.setMaterials(materials);
            objLoader.load(this.objPath, model => {
                this.model = model;
                if(this.game){
                    this.game.scene.add(model);
                }
                this.onModelInitialized();
            });
        });


    }

    onModelInitialized(){
        const object = this.model;
        window.o=object;
    }

    onAdd(game){
        super.onAdd(game);

        if(this.model){
            this.game.scene.add(this.model);
        }
    }
}
