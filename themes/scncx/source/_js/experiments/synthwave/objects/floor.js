import GameObject from './base';
import * as THREE from 'three';
import FSNoise from 'fast-simplex-noise';

export default class FloorObject extends GameObject {
    material = null;
    noiseFloor = null;
    plainFloor = null;
    mountains = [];
    tileSize = 100;

    constructor(){
        super();
        this.initPlainFloor();
        this.initMontains();
        //this.initNoiseFloor();
    }

    initPlainFloor(){
        const size = 10000;
        const tileAmount = size / this.tileSize;
        this.plainFloor = new THREE.Mesh(new THREE.PlaneBufferGeometry(size, size), this.getFloorMaterial(tileAmount));
        this.plainFloor.rotation.x = Math.PI / -2;
        this.plainFloor.position.y = -0.5;
    }

    initMontains(){
        const mountainSize = 1000;
        const tileAmount = mountainSize / this.tileSize;

        const m = THREE.Terrain({
            easing: THREE.Terrain.Linear,
            frequency: 2.5,
            heightmap: THREE.Terrain.HillIsland,
            material: new THREE.MultiMaterial([this.getFloorMaterial(tileAmount), new THREE.MeshBasicMaterial({ transparent: true, opacity: 0 })]),
            maxHeight: 200,
            minHeight: 0,
            steps: 1,
            turbulent: true,
            useBufferGeometry: false,
            xSegments: tileAmount,
            xSize: mountainSize,
            ySegments: tileAmount,
            ySize: mountainSize,
            after: (vertices, options) => {
                THREE.Terrain.RadialEdges(vertices, options, false, Math.min(options.xSize, options.ySize) * 0.5 - 100, THREE.Terrain.EaseInOut);
            }
        });

        const geometry = m.children[0].geometry;
        const epsilon = 1;

        // hide faces on the floor
        geometry.faces.forEach(face => {
            const a = geometry.vertices[face.a];
            const b = geometry.vertices[face.b];
            const c = geometry.vertices[face.c];

            if(a.z <= epsilon && b.z <= epsilon && c.z <= epsilon){
                face.materialIndex = 1;
            }
        });

        m.position.x = 2000;

        geometry.sortFacesByMaterialIndex();
        this.mountains.push(m);
    }

    getFloorMaterial(repeatX = 128, repeatY = repeatX){
        const texture = new THREE.TextureLoader().load('/images/retro-grid.jpg');
        texture.wrapS = THREE.RepeatWrapping;
        texture.wrapT = THREE.RepeatWrapping;
        texture.repeat.set(repeatX, repeatY);
        texture.offset.set(0.5, 0.5);
        return new THREE.MeshBasicMaterial({
            color: 0xffffff,
            map: texture
        });
    }

    onAdd(game){
        super.onAdd(game);
        this.game.scene.add(this.plainFloor);
        this.mountains.forEach(mountain => this.game.scene.add(mountain));
    }

    update(delta){
        //this.mesh.position.z += 0.1;
    }
}
