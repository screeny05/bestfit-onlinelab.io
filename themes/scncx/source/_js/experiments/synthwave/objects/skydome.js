import GameObject from './base';
import * as THREE from 'three';
import SkydomeShader from '../shader/skydome';

export default class SkydomeObject extends GameObject {
    dome = null;

    constructor(){
        super();
        const material = new THREE.ShaderMaterial({ ...SkydomeShader, side: THREE.BackSide });
        this.dome = new THREE.Mesh(new THREE.SphereGeometry(4000, 32, 7, 0, Math.PI * 2, 0, Math.PI / 2), material);
    }

    onAdd(game){
        super.onAdd(game);
        this.game.scene.add(this.dome);
    }
}
