import debounce from 'throttle-debounce/debounce';

export default class Controls {
    kbdValues = {
        37: 'left',
        39: 'right',
        38: 'forward',
        40: 'backward',
        65: 'left',
        68: 'right',
        83: 'backward',
        87: 'forward',
    };

    kbdToggles = {
        16: 'isSprinting'
    };

    states = {
        left: 0,
        right: 0,
        forward: 0,
        backward: 0,
        vertical : 0,
        horizontal: 0,
        isSprinting: false
    };

    get hasPointerLock(){
        return document.pointerLockElement === this.element;
    }

    constructor(element){
        this.element = element;
        this.registerEvents();
    }


    registerEvents(){
        this.element.addEventListener('keydown', this.onKey.bind(this, true), false);
        this.element.addEventListener('keyup', this.onKey.bind(this, false), false);
        this.element.addEventListener('mousemove', ::this.onMouseMove, false);
        this.element.addEventListener('mousemove', debounce(100, ::this.onMouseMoveEnd), false);
        this.element.addEventListener('click', ::this.onMouseClick, false);
    }

    onKey(isPressed, e){
        const stateValueId = this.kbdValues[e.keyCode];
        const stateToggleId = this.kbdToggles[e.keyCode];

        if(stateValueId){
            this.states[stateValueId] = isPressed ? 1 : 0;
        }

        if(stateToggleId){
            this.states[stateToggleId] = isPressed;
        }

        if(stateValueId || stateToggleId){
            e.preventDefault();
            e.stopPropagation();
        }
    }

    onMouseMove(e){
        if(!this.hasPointerLock){
            return;
        }
        this.states.vertical = e.movementY / this.element.offsetHeight * -100;
        this.states.horizontal = e.movementX / this.element.offsetWidth * -100;
    }

    onMouseMoveEnd(){
        this.states.vertical = 0;
        this.states.horizontal = 0;
    }

    onMouseClick(){
        this.element.requestPointerLock();
    }
}
