import fragmentShader from './bad-tv.fs';
import vertexShader from './bad-tv.vs';

export default {
    uniforms: {
        tDiffuse: { type: "t", value: null },
        time: { type: "f", value: 0.0 },
        distortion: { type: "f", value: 0.5 },
        speed: { type: "f", value: 0.2 },
        rollSpeed: { type: "f", value: 0.1 },
        filmGrain: { type: "f", value: 10.1 },
        scanline: { type: "f", value: 1 },
        sCount: { type: "f", value: 4096 },
        rgbShiftAmount: { type: "f", value: 0.005 },
        rgbShiftAngle: { type: "f", value: 0 },
    },

    vertexShader,
    fragmentShader
};
