uniform sampler2D tDiffuse;
uniform float time;
uniform float distortion;
uniform float speed;
uniform float filmGrain;
uniform float scanline;
uniform float sCount;
uniform float rgbShiftAmount;
uniform float rgbShiftAngle;
varying vec2 vUv;


// ashima 2d simplex noise
vec3 mod289(vec3 x){
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec2 mod289(vec2 x){
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec3 permute(vec3 x){
    return mod289(((x * 34.0) + 1.0) * x);
}

float snoise(vec2 v){
    const vec4 C = vec4(0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439);

    vec2 i = floor(v + dot(v, C.yy));
    vec2 x0 = v - i + dot(i, C.xx);

    vec2 i1;
    i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
    vec4 x12 = x0.xyxy + C.xxzz;
    x12.xy -= i1;

    // Avoid truncation effects in permutation
    i = mod289(i);
    vec3 p = permute(permute(i.y + vec3(0.0, i1.y, 1.0)) + i.x + vec3(0.0, i1.x, 1.0));

    vec3 m = max(0.5 - vec3(dot(x0, x0), dot(x12.xy, x12.xy), dot(x12.zw, x12.zw)), 0.0);
    m = m * m;
    m = m * m;

    vec3 x = 2.0 * fract(p * C.www) - 1.0;
    vec3 h = abs(x) - 0.5;
    vec3 ox = floor(x + 0.5);
    vec3 a0 = x - ox;

    m *= 1.79284291400159 - 0.85373472095314 * (a0 * a0 + h * h);

    vec3 g;
    g.x = a0.x * x0.x + h.x * x0.y;
    g.yz = a0.yz * x12.xz + h.yz * x12.yw;
    return 130.0 * dot(m, g);
}

void main(){
    float timeFactor = vUv.y - time * speed;

    // fine distortion
    float offset = snoise(vec2(timeFactor * 50.0, 0.0)) * distortion * 0.001;

    // combine offset x
    vec4 textureScreen = texture2D(tDiffuse, vec2(fract(vUv.x + offset), vUv.y));

    /*float x = vUv.x * vUv.y * timeFactor *  1000.0;
    x = mod(x, 13.0) * mod(x, 123.0);
    float dx = mod(x, 0.01);
    vec3 cResult = textureScreen.rgb + textureScreen.rgb * clamp(0.1 + dx * 100.0, 0.0, 1.0);
    vec2 sc = vec2(sin(vUv.y * sCount), cos(vUv.y * sCount));
    cResult += textureScreen.rgb * vec3(sc.x, sc.y, sc.x) * scanline;
    cResult = textureScreen.rgb + clamp(filmGrain, 0.0, 1.0) * (cResult - textureScreen.rgb);

    gl_FragColor = vec4(cResult, textureScreen.a);*/
    gl_FragColor = textureScreen;
}
