import * as THREE from 'three';

import fragmentShader from './skydome.fs';
import vertexShader from './skydome.vs';

export default {
    uniforms: {
        topColor: { value: new THREE.Color(0x221c31) },
        bottomColor: { value: new THREE.Color(0xe54aaf) },
        offset: { value: 30 },
        exponent: { value: 0.15 }
    },

    vertexShader,
    fragmentShader
};
