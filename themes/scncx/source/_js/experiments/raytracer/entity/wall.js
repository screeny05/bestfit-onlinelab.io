import Entity from './entity';
import Vector2 from '../lib/vector2';

export default class Wall extends Entity {
    hasCollisions = true;
    drawHeight = 1;

    constructor(position, game){
        super(position, game);
        this.setData();
    }

    update(delta){
        super.update(delta);
    }

    setData(){
        this.points = [
            this.position,
            this.position.add(1, 0),
            this.position.add(1, 1),
            this.position.add(0, 1)
        ];

        this.polygons = [
            { drawHeight: this.drawHeight, points: [this.points[0], this.points[1]], normal: Math.PI * 1.5, origin: this.points[0].lerp(this.points[1], .5) },
            { drawHeight: this.drawHeight, points: [this.points[1], this.points[2]], normal: 0, origin: this.points[1].lerp(this.points[2], .5) },
            { drawHeight: this.drawHeight, points: [this.points[2], this.points[3]], normal: Math.PI * 0.5, origin: this.points[2].lerp(this.points[3], .5) },
            { drawHeight: this.drawHeight, points: [this.points[3], this.points[0]], normal: Math.PI, origin: this.points[3].lerp(this.points[0], .5) },
        ];
    }

    getPolygons(){

    }
}
