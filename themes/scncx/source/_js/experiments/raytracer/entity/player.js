import Vector2 from '../lib/vector2';
import Random from '../lib/random';
import MathE from '../lib/math-e';
import Entity from './entity';

export default class Player extends Entity {
    moveSpeedForwards = 3;
    moveSpeedSidewards = 2;
    moveSpeedSprinting = 5;
    sprintingRemainingDuration = 0;
    sprintingAvailableDuration = 4;
    sprintingRefillFactor = .5;
    hasCollisions = true;

    constructor(game){
        super(game);
        this.sprintingRemainingDuration = this.sprintingAvailableDuration;
    }

    update(delta){
        super.update(delta);

        if(this.game.controls.states.left !== 0){
            this.move(this.moveSpeedSidewards * this.game.controls.states.left * delta, this.direction + 0.5 * Math.PI);
        }

        if(this.game.controls.states.right !== 0){
            this.move(this.moveSpeedSidewards * this.game.controls.states.right * delta, this.direction + 1.5 * Math.PI);
        }

        if(this.game.controls.states.forward !== 0){
            let speed = this.moveSpeedForwards * this.game.controls.states.forward * delta;

            if(this.game.controls.states.isSprinting && this.sprintingRemainingDuration > 0){
                speed = this.moveSpeedSprinting * this.game.controls.states.forward * delta;
                this.sprintingRemainingDuration -= delta;
            }

            this.move(speed, this.direction);
        }

        if(this.game.controls.states.backward !== 0){
            this.move(-this.moveSpeedForwards * this.game.controls.states.backward * delta, this.direction);
        }

        if(this.game.controls.states.rotation !== 0){
            this.direction += Math.PI * this.game.controls.states.rotation * delta;
        }

        if(!this.game.controls.states.isSprinting && this.sprintingRemainingDuration <= this.sprintingAvailableDuration){
            this.sprintingRemainingDuration += delta * this.sprintingRefillFactor;
        }
    }
}
