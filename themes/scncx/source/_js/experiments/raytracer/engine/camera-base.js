export default class CameraBase {
    constructor(game){
        this.game = game;
    }

    get position(){
        return this.game.player.position;
    }

    get direction(){
        return this.game.player.direction;
    }

    draw(){
        throw new Error(`draw not implemented by camera`);
    }
}
