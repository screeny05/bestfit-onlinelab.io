import debounce from 'throttle-debounce/debounce';

export default class Controls {
    kbdValues = {
        37: 'left',
        39: 'right',
        38: 'forward',
        40: 'backward',
        65: 'left',
        68: 'right',
        83: 'backward',
        87: 'forward',
    };

    kbdToggles = {
        16: 'isSprinting'
    };

    states = {
        left: 0,
        right: 0,
        forward: 0,
        backward: 0,
        rotation: 0,
        isSprinting: false
    };

    constructor(game){
        this.game = game;
        this.registerEvents();
    }


    registerEvents(){
        document.addEventListener('keydown', this.onKey.bind(this, true), false);
        document.addEventListener('keyup', this.onKey.bind(this, false), false);
        document.addEventListener('mousemove', ::this.onMouseMove, false);
        document.addEventListener('mousemove', debounce(100, ::this.onMouseMoveEnd), false);
    }

    onKey(isPressed, e){
        const stateValueId = this.kbdValues[e.keyCode];
        const stateToggleId = this.kbdToggles[e.keyCode];

        if(stateValueId){
            this.states[stateValueId] = isPressed ? 1 : 0;
        }

        if(stateToggleId){
            this.states[stateToggleId] = isPressed;
        }

        if(stateValueId || stateToggleId){
            e.preventDefault();
            e.stopPropagation();
        }
    }

    onMouseMove(e){
        this.states.rotation = e.movementX / window.innerWidth * -100;
    }

    onMouseMoveEnd(){
        this.states.rotation = 0;
    }
}
