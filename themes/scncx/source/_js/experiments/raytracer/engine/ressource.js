const resCache = new Map();

export default class Ressource {
    TYPE = 'Unknown';

    hasLoaded = false;
    isLoading = false;
    isSuccess = null;
    data = null;
    meta = {};

    constructor(id, url){
        this.id = id;
        this.url = url;

        resCache.set(id, this);
        this.load();
    }

    load(){
        this.isLoading = true;
        this.meta.loadingTimeStart = Date.now();

        return isSuccess => {
            if(this.hasLoaded){
                return false;
            }

            this.isSuccess = isSuccess;
            this.hasLoaded = true;
            this.isLoading = false;
            this.meta.loadingTimeEnd = Date.now();
            this.meta.loadingDuration = this.meta.loadingTimeEnd - this.meta.loadingTimeStart;

            if(!isSuccess){
                console.warn(`couldn't load ressource ${this.id}`);
                return false;
            }

            return true;
        };
    }
}

export class ImageRessource extends Ressource {
    TYPE = 'Image';

    load(){
        const img = new Image();
        const superHandler = super.load();
        const handler = isSuccess => {
            if(!superHandler(isSuccess)){
                return;
            }

            this.data = img;
            this.meta.width = img.width;
            this.meta.height = img.height;
        };

        img.onload = handler.bind(null, true);
        img.onerror = handler.bind(null, false);
        img.onabort = handler.bind(null, false);
        img.oncancel = handler.bind(null, false);
        img.src = this.url;
    }
}

export class BinaryRessource extends Ressource {
    TYPE = 'Binary';

    load(){
        const xhr = new XMLHttpRequest();
        const superHandler = super.load();
        const handler = isSuccess => {
            isSuccess = isSuccess && xhr.status === 200;

            if(!superHandler(isSuccess)){
                return;
            }

            this.data = xhr.response;
        };

        xhr.onloadend = handler.bind(null, true);
        xhr.onerror = handler.bind(null, false);
        xhr.onabort = handler.bind(null, false);
        xhr.ontimeout = handler.bind(null, false);

        xhr.open(this.url, 'GET');
        xhr.send();
    }
}
