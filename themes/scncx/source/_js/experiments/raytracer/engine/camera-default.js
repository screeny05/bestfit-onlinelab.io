import MathE from '../lib/math-e';
import Vector2 from '../lib/vector2';
import Color from '../lib/color';

import CameraBase from './camera-base';

export default class CameraDefault extends CameraBase {
    focalHeight = 1;
    viewAngle = MathE.degToRad(100);
    maxViewDistance = 15;
    minViewDistance = 0;
    eyeHeight = .4;

    constructor(game){
        super(game);
    }

    draw(){
        this.game.draw.clear();
        this.drawSky();
        this.drawFloor();
        this.drawScene();
        this.drawPlayer();
        //this.drawOverview();
    }

    drawSky(){
        this.game.draw.angledImage(this.game.scene.skyRes, new Vector2(), this.game.canvasWidth, this.game.canvasHeight, this.direction, this.viewAngle);
    }

    drawFloor(){
        this.game.draw.linearGradient(
            new Vector2(0, this.game.canvasHeight / 2),
            this.game.canvasWidth, this.game.canvasHeight / 2,
            [
                new Color(0, 0, 0),
                { offset: .1, color: new Color(0, 0, 0) },
                { offset: .4, color: new Color(80, 80, 80) },
                new Color(180, 180, 180)
            ]
        );
    }

    drawScene(){
        const polygons = Array.prototype.concat.apply([], this.game.scene.entities
            .filter(entity => this.isVectorInViewDistance(entity.position) && this.isVectorInViewField(entity.points))
            .map(entity => entity.polygons)
        );

        polygons
            .filter(polygon => this.isPolygonFaceVisible(polygon))
            .map(polygon => this.projectPolygon(polygon))
            .sort((a, b) => b.distance - a.distance)
            .forEach(plane => this.drawPlane(plane));
    }

    isPolygonFaceVisible(polygon){
        return MathE.isAngleWithin(this.position.angleToVector(polygon.origin), polygon.normal - Math.PI * .5, polygon.normal + Math.PI * .5);
    }

    shouldInvertAngle(polygon, point, angleToPlayer){
        const isPointInView = Math.abs(angleToPlayer) < this.viewAngle / 2;
        if(isPointInView){
            // early exit
            return false;
        }

        const angleToPoints = polygon.points.map(point => this.position.angleToVector(point));
        const anglePointA = MathE.wrapAngleRad(angleToPoints[0] + Math.PI);
        const anglePointB = MathE.wrapAngleRad(angleToPoints[1] + Math.PI);
        const inverseViewAngle = MathE.wrapAngleRad(this.direction + Math.PI);

        return MathE.isAngleWithin(inverseViewAngle, anglePointB, anglePointA);
    }

    projectPolygon(polygon){
        const projectedPoints = [];
        let accumulatedDistance = 0;

        polygon.points.forEach((point, i) => {
            // position of the point relative to the camera
            const pointRelative = point.subtract(this.position);

            // angle relative to the cameras direction (-0.5 viewport to 0.5 viewport)
            let angleToPlayer = MathE.angleBetweenAngles(this.direction, pointRelative.toPolar());

            if(this.shouldInvertAngle(polygon, point, angleToPlayer)){
                angleToPlayer = -angleToPlayer;
            }

            // correct distance for fisheye effect
            const pointMagnitude = pointRelative.getMagnitude();
            const pointDistance = pointMagnitude / Math.cosh(angleToPlayer);

            // height and x/y-position of the line on screen
            const drawHeight = this.game.canvasHeight / pointDistance * this.focalHeight * polygon.drawHeight;
            const drawY = this.game.canvasHeight / 2 - drawHeight * this.eyeHeight;
            const drawX = (this.viewAngle / 2 + angleToPlayer) / this.viewAngle * this.game.canvasWidth;

            accumulatedDistance += pointMagnitude;

            projectedPoints.push({
                top: new Vector2(drawX, drawY),
                bottom: new Vector2(drawX, drawY + drawHeight)
            });
        });

        return {
            points: [projectedPoints[0].top, projectedPoints[0].bottom, projectedPoints[1].bottom, projectedPoints[1].top],
            distance: accumulatedDistance / polygon.points.length
        };
    }

    drawPlane(plane){
        const shade = MathE.lerp(200, 0, plane.distance / this.maxViewDistance);
        this.game.draw.polygon(new Color(shade, shade, shade), plane.points);
    }

    drawPlayer(){}

    drawOverview(){
        const scale = 20;
        const leftBase = this.game.canvasWidth / 2;
        const bottomBase = this.game.canvasHeight / 2;

        this.game.ctx.save();
        this.game.ctx.translate(leftBase, bottomBase);
        this.game.ctx.scale(1, -1);

        this.game.scene.entities.forEach(wall => {
            this.game.draw.rect(this.isVectorInViewDistance(wall.position) && this.isVectorInViewField(wall.points) ? '#000' : '#777', wall.position.multiply(scale), scale, scale);
        });

        this.game.draw.line('#333', this.game.player.position.multiply(scale), this.game.player.position.add(Vector2.fromPolar(this.maxViewDistance, this.direction)).multiply(scale));
        this.game.draw.line('#999', this.game.player.position.subtract(Vector2.fromPolar(this.maxViewDistance, this.direction)).multiply(scale), this.game.player.position.multiply(scale), [5]);
        this.game.draw.line('#eee', this.game.player.position.add(Vector2.fromPolar(this.maxViewDistance, this.direction - this.viewAngle / 2)).multiply(scale), this.game.player.position.multiply(scale), [5]);
        this.game.draw.line('#eee', this.game.player.position.add(Vector2.fromPolar(this.maxViewDistance, this.direction + this.viewAngle / 2)).multiply(scale), this.game.player.position.multiply(scale), [5]);

        this.game.draw.rect('#ffe600', this.game.player.position.multiply(scale));

        this.game.ctx.restore();
    }

    isVectorInViewDistance(worldPosition){
        const relativeDistance = worldPosition.subtract(this.position).getMagnitude();
        return relativeDistance < this.maxViewDistance && relativeDistance > this.minViewDistance;
    }

    isVectorInViewField(worldPosition){
        if(Array.isArray(worldPosition)){
            return worldPosition.some(pos => this.isVectorInViewField(pos));
        }

        const relativePosition = worldPosition.subtract(this.position);
        const angleToXAxis = relativePosition.toPolar();

        return MathE.isAngleWithin(angleToXAxis, this.direction - (this.viewAngle / 2), this.direction + (this.viewAngle / 2));
    }
}
