import Vector2 from '../lib/vector2';
import Random from '../lib/random';
import { ImageRessource } from './ressource';
import Wall from '../entity/wall';

export default class Scene {
    entities = [];
    skyRes = new ImageRessource('map-sky', 'https://raw.githubusercontent.com/hunterloftis/playfuljs-demos/gh-pages/raycaster/assets/deathvalley_panorama.jpg');

    constructor(game){
        this.game = game;
    }

    iteratePositions(width, height = width, iterator){
        for(let x = 0; x < width; x++) {
            for(let y = 0; y < height; y++) {
                iterator(x, y);
            }
        }
    }

    buildRandom(width, height = width){
        this.iteratePositions(width, height, (x, y) => {
            if(Math.random() > 0.7){
                this.entities.push(new Wall(this.game, new Vector2(x, y)));
            }
        });
    }

    buildFill(width, height = width){
        this.iteratePositions(width, height, (x, y) => {
            this.entities.push(new Wall(this.game, new Vector2(x, y)));
        });
    }

    buildBasic(){
        this.buildWalls(10, 5);
    }

    buildWalls(width, height = width){
        this.iteratePositions(width, height, (x, y) => {
            if(x === 0 || x === width - 1 || y === 0 || y === height - 1){
                this.entities.push(new Wall(this.game, new Vector2(x, y)));
            }
        });
    }

    getEntityAt(position){
        return this.entities.find(wall => wall.position.floor().equals(position.floor()));
    }

    update(){}
}
