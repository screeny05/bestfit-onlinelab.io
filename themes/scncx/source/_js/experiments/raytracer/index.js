import Game from './engine/game';

const game = new Game(document.getElementById('canvas'));
game.start();
