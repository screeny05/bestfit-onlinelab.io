export default class StringE {
    static leftPad(str, len, padding = '0'){
        return len - str.length > 0 ? this.repeat(padding, len - str.length) : str;
    }

    static repeat(str, times){
        let ret = '';

        for(let i = 0; i < times; i++) {
            ret += str;
        }

        return ret;
    }
}
