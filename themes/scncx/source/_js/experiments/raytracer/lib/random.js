export default class Random {
    static between(min = 0, max = 100, floor = true){
        if(floor){
            return Math.floor(Math.random() * (max - min + 1)) + min;
        } else {
            return Math.random() * (max - min) + min;
        }
    }

    static bool(){
        return Math.random() >= .5;
    }

    static fromArray(array){
        return array[Random.between(0, array.length - 1)];
    }
}
