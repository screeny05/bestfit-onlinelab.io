import MathE from './math-e';
import Color from './color';

export default class Draw {
    constructor(game) {
        this.game = game;
        this.ctx = this.game.ctx;
    }

    clear(){
        this.ctx.clearRect(0, 0, this.game.canvasWidth, this.game.canvasHeight);
    }

    line(color, a, b, lineDash){
        this.ctx.save();
        if(lineDash){
            this.ctx.setLineDash(lineDash);
        }
        this.ctx.strokeStyle = color;
        this.ctx.beginPath();
        this.ctx.moveTo(a.x, a.y);
        this.ctx.lineTo(b.x, b.y);
        this.ctx.stroke();
        this.ctx.restore();
    }

    rect(color, position, width = 1, height = width){
        this.ctx.save();
        this.ctx.fillStyle = color;
        this.ctx.fillRect(position.x, position.y, width, height);
        this.ctx.restore();
    }

    polygon(color, points){
        this.ctx.save();
        if(!points || points.length === 0){
            return;
        }

        this.ctx.beginPath();
        this.ctx.moveTo(points[0].x, points[0].y);

        points.slice(1).forEach(point => {
            this.ctx.lineTo(point.x, point.y);
        });

        this.ctx.fillStyle = color.toRGBA();

        this.ctx.fill();
        this.ctx.restore();
    }

    linearGradient(pos, w, h, stops){
        const gradient = this.ctx.createLinearGradient(pos.x, pos.y, pos.x, pos.y + h);
        stops.forEach((s, i) => {
            if(s instanceof Color){
                gradient.addColorStop(i / (stops.length - 1), s.toRGBA());
            } else {
                gradient.addColorStop(s.offset, s.color.toRGBA());
            }
        });

        this.ctx.save();
        this.ctx.fillStyle = gradient;
        this.ctx.fillRect(pos.x, pos.y, w, h);
        this.ctx.restore();
    }

    radialGradient(pos0, pos1, pos2, r1, r2, w, h, stops){
        const gradient = this.ctx.createRadialGradient(pos1.x, pos1.y, r1, pos2.x, pos2.y, r2);

        stops.forEach((s, i) => {
            gradient.addColorStop(s.offset, s.color.toRGBA());
        });

        this.ctx.save();
        this.ctx.fillStyle = gradient;
        this.ctx.fillRect(pos0.x, pos0.y, w, h);
        this.ctx.restore();
    }

    angledImage(imgRes, pos, w, h, angle, viewAngle){
        if(!imgRes.isSuccess){
            return;
        }

        this.ctx.save();

        const angleScreenFactor = viewAngle / MathE.PI2;
        const imageFactor = 1 / angleScreenFactor;
        const fullAngleWidth = imgRes.meta.width * imageFactor;
        const drawHeight = imgRes.meta.height * imageFactor;
        const percOfCircle = 1 - (angle / MathE.PI2);
        const left = -1 * percOfCircle * fullAngleWidth;

        this.ctx.drawImage(imgRes.data, left, 0, fullAngleWidth, h);

        if(-1 * (left - this.game.canvasWidth) > fullAngleWidth){
            this.ctx.drawImage(imgRes.data, left + fullAngleWidth, 0, fullAngleWidth, h);
        }

        this.ctx.restore();
    }
}
