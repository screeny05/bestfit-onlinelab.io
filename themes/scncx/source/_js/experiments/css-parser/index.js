class Stream {
    constructor(string = ''){
        this.cursor = 0;
        this.chars = string.split('');
    }

    isEOF(){
        return this.cursor >= this.chars.length - 1;
    }

    peek(length = 1){
        return length === 1 ? this.chars[this.cursor] : this.chars.slice(this.cursor, length).join('');
    }

    read(length = 1){
        const ret = this.peek(length);
        this.cursor += length;
        return ret;
    }

    readUntil(terminator, includeTerminator = true){
        let buffer = '';
        while(buffer.indexOf(terminator) === -1 && !this.isEOF()){
            buffer += this.read();
        }

        return includeTerminator ? buffer : buffer.replace(terminator, '');
    }

    parseLoop(fn){
        let nextStr = null;
        while(!this.isEOF()){
            nextStr = fn(nextStr);
        }
    }

    static isWhitespace(str){
        return /^\s*$/.test(str);
    }
}

const S_ROOT = 'S_ROOT';
const S_SELECTOR = 'S_SELECTOR';
const S_MEDIA_QUERY = 'S_MEDIA_QUERY';
const S_RULESET = 'S_RULESET';
const S_RULE = 'S_RULE';
const S_PROPERTY = 'S_PROPERTY';
const S_VALUE = 'S_VALUE';
const S_COMMENT = 'S_COMMENT';

const T_SELECTOR = 'T_SELECTOR';
const T_VALUE = 'T_VALUE';
const T_PROPERTY = 'T_PROPERTY';
const T_BRACKET_OPEN = 'T_BRACKET_OPEN';
const T_BRACKET_CLOSE = 'T_BRACKET_CLOSE';
const T_COMMENT = 'T_COMMENT';
const T_COMMENT_START = 'T_COMMENT_START';
const T_COMMENT_END = 'T_COMMENT_END';
const T_AT = 'T_AT';
const T_AT_RULE = 'T_AT_RULE';

class CSSParser {
    constructor(css = ''){
        this.css = css;
        this.stream = new Stream(css);
        this.tokens = [];
    }

    parse(){
        const states = [S_ROOT];
        const pushToken = (type, content, origin) => this.tokens.push({ type, content, origin });
        let buffer = '';


        this.stream.parseLoop(last => {
            const state = states[states.length - 1];
            const char = last ? last : this.stream.read();

            if(state === S_COMMENT){
                if(char === '*' && this.stream.peek() === '/'){
                    pushToken(T_COMMENT, buffer, S_COMMENT);
                    pushToken(T_COMMENT_END, null, S_COMMENT);
                    this.stream.read();
                    states.pop();
                    buffer = '';
                    return;
                }
                buffer += char;
                return;
            }

            if(char === '/' && this.stream.peek() === '*'){
                pushToken(T_COMMENT_START, null, state);
                states.push(S_COMMENT);
                this.stream.read();
                return;
            }

            if(char === '@' && (state === S_ROOT || state === S_RULESET || state === S_MEDIA_QUERY)){
                pushToken(T_AT, '@', state);
                states.push(S_MEDIA_QUERY);
                return;
            }

            if(state === S_MEDIA_QUERY){
                if(char === '{'){
                    pushToken(T_AT_RULE, buffer.trim(), S_MEDIA_QUERY);
                    pushToken(T_BRACKET_OPEN, '{', S_MEDIA_QUERY);
                    states.pop();
                    buffer = '';
                    return;
                }
                buffer += char;
                return;
            }

            if(state === S_VALUE){
                if(char === ';' || char === '}'){
                    pushToken(T_VALUE, buffer.trim(), S_VALUE);
                    buffer = '';
                    states.pop();

                    if(char === '}'){
                        pushToken(T_BRACKET_CLOSE, null, S_VALUE);
                        // exit S_RULESET
                        states.pop();
                    }

                    return;
                }

                buffer += char;
                return;
            }

            if(state === S_PROPERTY){
                if(char === ';' || char === '}'){
                    buffer = '';
                    states.pop();
                    if(char === '}'){
                        pushToken(T_BRACKET_CLOSE, null, S_PROPERTY);
                        states.pop();
                    }
                    return;
                }

                if(char === ':'){
                    pushToken(T_PROPERTY, buffer.trim(), S_PROPERTY);
                    buffer = '';
                    states.pop();
                    states.push(S_VALUE);
                    return;
                }

                buffer += char;
                return;
            }

            if(state === S_RULESET){
                if(char === '}'){
                    pushToken(T_BRACKET_CLOSE, null, S_RULESET);
                    states.pop();
                    return;
                }

                states.push(S_PROPERTY);
                return char;
            }

            if(state === S_SELECTOR){
                if(char === ',' || char === '{'){
                    pushToken(T_SELECTOR, buffer.trim(), S_SELECTOR);
                    states.pop();
                    buffer = '';

                    if(char === '{'){
                        pushToken(T_BRACKET_OPEN, null, S_SELECTOR);
                        states.push(S_RULESET);
                    }

                    return;
                }

                buffer += char;
                return;
            }

            if(state === S_ROOT && !Stream.isWhitespace(char)){
                states.push(S_SELECTOR);
                return char;
            }
        });

        return states;
    }
}


const c = new CSSParser(`
@media foo and (min-width: bar){
    .foobar {
        /*qux: baz;*/
    }
}
`);

const beautify = tokens => {
    return tokens.map((token, i) => {
        if(token.type === T_SELECTOR){
            const addComma = tokens[i + 1].type === T_SELECTOR;
            return token.content + (addComma ? ',\n' : ' ');
        }

        if(token.type === T_VALUE){
            return token.content + ';\n';
        }

        if(token.type === T_PROPERTY){
            return '\t' + token.content + ': ';
        }

        if(token.type === T_BRACKET_OPEN){
            return '{\n';
        }

        if(token.type === T_BRACKET_CLOSE){
            return '}\n\n';
        }

        return '';
    }).join('').trim();
}

const uglify = tokens => {
    return tokens.map((token, i) => {
        if(token.type === T_SELECTOR){
            const addComma = tokens[i + 1].type === T_SELECTOR;
            return token.content + (addComma ? ',' : '');
        }

        if(token.type === T_VALUE){
            const addSemicolon = tokens[i + 1].type === T_PROPERTY;
            return token.content + (addSemicolon ? ';' : '');
        }

        if(token.type === T_PROPERTY){
            return token.content + ':';
        }

        if(token.type === T_BRACKET_OPEN){
            return '{';
        }

        if(token.type === T_BRACKET_CLOSE){
            return '}';
        }

        return '';
    }).join('').trim();
}

console.log(c.parse());
console.log(c.tokens);
//console.log(uglify(c.tokens));
//console.log(beautify(c.tokens));
