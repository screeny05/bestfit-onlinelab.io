import * as MathE from './core';
import Vector2 from './vector2';

export function between(start, end, precision = 0){
    if(start > end){
        [start, end] = [end, start];
    }

    return MathE.roundPrecise(start + Math.random() * (end - start), precision);
};


export function vectorOnDisc(radiusX, radiusY = radiusX){
    let randomA = Math.random();
    let randomB = Math.random();

    if(randomB < randomA){
        [randomB, randomA] = [randomA, randomB];
    }

    return new Vector2(
        randomB * radiusX * Math.cos(MathE.PI2 * randomA / randomB),
        randomB * radiusY * Math.sin(MathE.PI2 * randomA / randomB)
    );
};


export function vectorOnCircle(radiusX, radiusY = radiusX){
    const angle = Math.random() * MathE.PI2;

    return new Vector2(
        Math.cos(angle) * radiusX,
        Math.sin(angle) * radiusY
    );
};
