/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 46);
/******/ })
/************************************************************************/
/******/ ({

/***/ 46:
/***/ function(module, exports) {

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Stream = function () {
    function Stream() {
        var string = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

        _classCallCheck(this, Stream);

        this.cursor = 0;
        this.chars = string.split('');
    }

    _createClass(Stream, [{
        key: 'isEOF',
        value: function isEOF() {
            return this.cursor >= this.chars.length - 1;
        }
    }, {
        key: 'peek',
        value: function peek() {
            var length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

            return length === 1 ? this.chars[this.cursor] : this.chars.slice(this.cursor, length).join('');
        }
    }, {
        key: 'read',
        value: function read() {
            var length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

            var ret = this.peek(length);
            this.cursor += length;
            return ret;
        }
    }, {
        key: 'readUntil',
        value: function readUntil(terminator) {
            var includeTerminator = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

            var buffer = '';
            while (buffer.indexOf(terminator) === -1 && !this.isEOF()) {
                buffer += this.read();
            }

            return includeTerminator ? buffer : buffer.replace(terminator, '');
        }
    }, {
        key: 'parseLoop',
        value: function parseLoop(fn) {
            var nextStr = null;
            while (!this.isEOF()) {
                nextStr = fn(nextStr);
            }
        }
    }], [{
        key: 'isWhitespace',
        value: function isWhitespace(str) {
            return (/^\s*$/.test(str)
            );
        }
    }]);

    return Stream;
}();

var S_ROOT = 'S_ROOT';
var S_SELECTOR = 'S_SELECTOR';
var S_MEDIA_QUERY = 'S_MEDIA_QUERY';
var S_RULESET = 'S_RULESET';
var S_RULE = 'S_RULE';
var S_PROPERTY = 'S_PROPERTY';
var S_VALUE = 'S_VALUE';
var S_COMMENT = 'S_COMMENT';

var T_SELECTOR = 'T_SELECTOR';
var T_VALUE = 'T_VALUE';
var T_PROPERTY = 'T_PROPERTY';
var T_BRACKET_OPEN = 'T_BRACKET_OPEN';
var T_BRACKET_CLOSE = 'T_BRACKET_CLOSE';
var T_COMMENT = 'T_COMMENT';
var T_COMMENT_START = 'T_COMMENT_START';
var T_COMMENT_END = 'T_COMMENT_END';
var T_AT = 'T_AT';
var T_AT_RULE = 'T_AT_RULE';

var CSSParser = function () {
    function CSSParser() {
        var css = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

        _classCallCheck(this, CSSParser);

        this.css = css;
        this.stream = new Stream(css);
        this.tokens = [];
    }

    _createClass(CSSParser, [{
        key: 'parse',
        value: function parse() {
            var _this = this;

            var states = [S_ROOT];
            var pushToken = function pushToken(type, content, origin) {
                return _this.tokens.push({ type: type, content: content, origin: origin });
            };
            var buffer = '';

            this.stream.parseLoop(function (last) {
                var state = states[states.length - 1];
                var char = last ? last : _this.stream.read();

                if (state === S_COMMENT) {
                    if (char === '*' && _this.stream.peek() === '/') {
                        pushToken(T_COMMENT, buffer, S_COMMENT);
                        pushToken(T_COMMENT_END, null, S_COMMENT);
                        _this.stream.read();
                        states.pop();
                        buffer = '';
                        return;
                    }
                    buffer += char;
                    return;
                }

                if (char === '/' && _this.stream.peek() === '*') {
                    pushToken(T_COMMENT_START, null, state);
                    states.push(S_COMMENT);
                    _this.stream.read();
                    return;
                }

                if (char === '@' && (state === S_ROOT || state === S_RULESET || state === S_MEDIA_QUERY)) {
                    pushToken(T_AT, '@', state);
                    states.push(S_MEDIA_QUERY);
                    return;
                }

                if (state === S_MEDIA_QUERY) {
                    if (char === '{') {
                        pushToken(T_AT_RULE, buffer.trim(), S_MEDIA_QUERY);
                        pushToken(T_BRACKET_OPEN, '{', S_MEDIA_QUERY);
                        states.pop();
                        buffer = '';
                        return;
                    }
                    buffer += char;
                    return;
                }

                if (state === S_VALUE) {
                    if (char === ';' || char === '}') {
                        pushToken(T_VALUE, buffer.trim(), S_VALUE);
                        buffer = '';
                        states.pop();

                        if (char === '}') {
                            pushToken(T_BRACKET_CLOSE, null, S_VALUE);
                            // exit S_RULESET
                            states.pop();
                        }

                        return;
                    }

                    buffer += char;
                    return;
                }

                if (state === S_PROPERTY) {
                    if (char === ';' || char === '}') {
                        buffer = '';
                        states.pop();
                        if (char === '}') {
                            pushToken(T_BRACKET_CLOSE, null, S_PROPERTY);
                            states.pop();
                        }
                        return;
                    }

                    if (char === ':') {
                        pushToken(T_PROPERTY, buffer.trim(), S_PROPERTY);
                        buffer = '';
                        states.pop();
                        states.push(S_VALUE);
                        return;
                    }

                    buffer += char;
                    return;
                }

                if (state === S_RULESET) {
                    if (char === '}') {
                        pushToken(T_BRACKET_CLOSE, null, S_RULESET);
                        states.pop();
                        return;
                    }

                    states.push(S_PROPERTY);
                    return char;
                }

                if (state === S_SELECTOR) {
                    if (char === ',' || char === '{') {
                        pushToken(T_SELECTOR, buffer.trim(), S_SELECTOR);
                        states.pop();
                        buffer = '';

                        if (char === '{') {
                            pushToken(T_BRACKET_OPEN, null, S_SELECTOR);
                            states.push(S_RULESET);
                        }

                        return;
                    }

                    buffer += char;
                    return;
                }

                if (state === S_ROOT && !Stream.isWhitespace(char)) {
                    states.push(S_SELECTOR);
                    return char;
                }
            });

            return states;
        }
    }]);

    return CSSParser;
}();

var c = new CSSParser('\n@media foo and (min-width: bar){\n    .foobar {\n        /*qux: baz;*/\n    }\n}\n');

var beautify = function beautify(tokens) {
    return tokens.map(function (token, i) {
        if (token.type === T_SELECTOR) {
            var addComma = tokens[i + 1].type === T_SELECTOR;
            return token.content + (addComma ? ',\n' : ' ');
        }

        if (token.type === T_VALUE) {
            return token.content + ';\n';
        }

        if (token.type === T_PROPERTY) {
            return '\t' + token.content + ': ';
        }

        if (token.type === T_BRACKET_OPEN) {
            return '{\n';
        }

        if (token.type === T_BRACKET_CLOSE) {
            return '}\n\n';
        }

        return '';
    }).join('').trim();
};

var uglify = function uglify(tokens) {
    return tokens.map(function (token, i) {
        if (token.type === T_SELECTOR) {
            var addComma = tokens[i + 1].type === T_SELECTOR;
            return token.content + (addComma ? ',' : '');
        }

        if (token.type === T_VALUE) {
            var addSemicolon = tokens[i + 1].type === T_PROPERTY;
            return token.content + (addSemicolon ? ';' : '');
        }

        if (token.type === T_PROPERTY) {
            return token.content + ':';
        }

        if (token.type === T_BRACKET_OPEN) {
            return '{';
        }

        if (token.type === T_BRACKET_CLOSE) {
            return '}';
        }

        return '';
    }).join('').trim();
};

console.log(c.parse());
console.log(c.tokens);
//console.log(uglify(c.tokens));
//console.log(beautify(c.tokens));

/***/ }

/******/ });
//# sourceMappingURL=experiments.css-parser.js.map