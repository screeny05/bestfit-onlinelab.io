/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 47);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */,
/* 1 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MathE = function () {
    function MathE() {
        _classCallCheck(this, MathE);
    }

    _createClass(MathE, null, [{
        key: "degToRad",
        value: function degToRad(deg) {
            return deg * MathE.DEG_TO_RAD_FACTOR;
        }
    }, {
        key: "radToDeg",
        value: function radToDeg(rad) {
            return rad * MathE.RAD_TO_DEG_FACTOR;
        }
    }, {
        key: "clamp",
        value: function clamp(val, min, max) {
            return Math.min(Math.max(val, min), max);
        }
    }, {
        key: "wrap",
        value: function wrap(val, min, max) {
            var range = max - min;
            if (range <= 0) {
                return 0;
            }
            var result = (val - min) % range;
            if (result < 0) {
                result += range;
            }
            return result + min;
        }
    }, {
        key: "wrapAngleRad",
        value: function wrapAngleRad(val) {
            return MathE.wrap(val, 0, MathE.PI2);
        }
    }, {
        key: "angleBetweenAngles",
        value: function angleBetweenAngles(val1, val2) {
            return Math.atan2(Math.sin(val1 - val2), Math.cos(val1 - val2));
        }
    }, {
        key: "isWithin",
        value: function isWithin(val, min, max) {
            return val <= max && val >= min;
        }
    }, {
        key: "isWrappingWithin",
        value: function isWrappingWithin(val, min, max, wrap) {
            val = (wrap + val % wrap) % wrap;
            min = (wrap * 10000 + min) % wrap;
            max = (wrap * 10000 + max) % wrap;
            if (min < max) {
                return min <= val && val <= max;
            } else {
                return min <= val || val <= max;
            }
        }
    }, {
        key: "isAngleWithin",
        value: function isAngleWithin(val, min, max) {
            return MathE.isWrappingWithin(val, min, max, MathE.PI2);
        }
    }, {
        key: "lerp",
        value: function lerp(start, end, t) {
            return start + (end - start) * t;
        }
    }, {
        key: "isNear",
        value: function isNear(val, target) {
            var threshold = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : MathE.EPSILON;

            return val < target ? val + threshold >= target : val - threshold <= target;
        }
    }, {
        key: "norm",
        value: function norm(val, min, max) {
            return (val - min) / (max - min);
        }
    }, {
        key: "map",
        value: function map(val, min1, max1, min2, max2) {
            return MathE.lerp(min2, max2, MathE.norm(val, min1, max1));
        }
    }, {
        key: "round",
        value: function round(val) {
            var digits = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

            if (digits === 0) {
                return Math.round(number);
            }

            var exponent = Math.pow(10, digits + 1);
            return Math.round(number * exponent) / exponent;
        }
    }]);

    return MathE;
}();

MathE.PI2 = Math.PI * 2;
MathE.PI_HALF = Math.PI / 2;
MathE.DEG_TO_RAD_FACTOR = Math.PI / 180;
MathE.RAD_TO_DEG_FACTOR = 180 / Math.PI;
MathE.EPSILON = 0.000001;
/* harmony default export */ exports["a"] = MathE;

/***/ },
/* 2 */,
/* 3 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__math_e__ = __webpack_require__(1);
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Vector2 = function () {
    function Vector2() {
        var x = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var y = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

        _classCallCheck(this, Vector2);

        this.x = 0;
        this.y = 0;

        this.x = x;
        this.y = y;
    }

    _createClass(Vector2, [{
        key: 'add',
        value: function add(a, b) {
            if (Vector2.isVector(a)) {
                return new Vector2(this.x + a.x, this.y + a.y);
            }

            return new Vector2(this.x + a, this.y + b);
        }
    }, {
        key: 'subtract',
        value: function subtract(v) {
            return new Vector2(this.x - v.x, this.y - v.y);
        }
    }, {
        key: 'multiply',
        value: function multiply(scaleX) {
            var scaleY = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : scaleX;

            return new Vector2(this.x * scaleX, this.y * scaleY);
        }
    }, {
        key: 'divide',
        value: function divide(divisor) {
            return new Vector2(this.x / divisor, this.y / divisor);
        }
    }, {
        key: 'floor',
        value: function floor() {
            return new Vector2(Math.floor(this.x), Math.floor(this.y));
        }
    }, {
        key: 'ceil',
        value: function ceil() {
            return new Vector2(Math.ceil(this.x), Math.ceil(this.y));
        }
    }, {
        key: 'equals',
        value: function equals(v) {
            return this.x === v.x && this.y === v.y;
        }
    }, {
        key: 'dot',
        value: function dot(v) {
            return this.x * v.x + this.y * v.y;
        }
    }, {
        key: 'normalize',
        value: function normalize() {
            var magnitude = this.getMagnitude();
            return new Vector2(this.x / magnitude, this.y / magnitude);
        }
    }, {
        key: 'angleToVector',
        value: function angleToVector(v) {
            return Math.atan2(this.y - v.y, this.x - v.x);
        }
    }, {
        key: 'toPolar',
        value: function toPolar() {
            return Math.atan2(this.y, this.x);
        }
    }, {
        key: 'moveTowardsVector',
        value: function moveTowardsVector(to, maxDistanceDelta) {
            return to.subtract(this).normalize().multiply(maxDistanceDelta).add(this);
        }
    }, {
        key: 'lerp',
        value: function lerp(to, t) {
            return new Vector2(__WEBPACK_IMPORTED_MODULE_0__math_e__["a" /* default */].lerp(this.x, to.x, t), __WEBPACK_IMPORTED_MODULE_0__math_e__["a" /* default */].lerp(this.y, to.y, t));
        }
    }, {
        key: 'getMagnitude',
        value: function getMagnitude() {
            return Math.sqrt(this.getSqrMagnitude());
        }
    }, {
        key: 'getSqrMagnitude',
        value: function getSqrMagnitude() {
            return this.x * this.x + this.y * this.y;
        }
    }, {
        key: Symbol.toStringTag,
        get: function get() {
            return 'Vector2(' + this.x + ', ' + this.y + ')';
        }
    }], [{
        key: 'fromIndex',
        value: function fromIndex(i, size) {
            return new Vector2(i % size, Math.floor(i / size));
        }
    }, {
        key: 'fromPolar',
        value: function fromPolar(length, angle) {
            return new Vector2(length * Math.cos(angle), length * Math.sin(angle));
        }
    }, {
        key: 'isVector',
        value: function isVector(v) {
            return (typeof v === 'undefined' ? 'undefined' : _typeof(v)) === 'object' && typeof v.x === 'number' && typeof v.y === 'number';
        }
    }, {
        key: 'getAverageMagnitude',
        value: function getAverageMagnitude(vectors) {
            var average = 0;
            vectors.forEach(function (v) {
                return average += v.getMagnitude();
            });
            return average / vectors.length;
        }
    }, {
        key: 'getAverageRelativeMagnitude',
        value: function getAverageRelativeMagnitude(origin, vectors) {
            return Vector2.getAverageMagnitude(vectors.map(function (v) {
                return v.subtract(origin);
            }));
        }
    }, {
        key: 'getAverageVector',
        value: function getAverageVector(vectors) {
            if (vectors.length === 0) {
                return new Vector2();
            }

            var averageVector = vectors[0];
            vectors.slice(1).forEach(function (v) {
                return averageVector = averageVector.add(v);
            });
            return averageVector.divide(vectors.length);
        }
    }, {
        key: 'getAverageRelativeVector',
        value: function getAverageRelativeVector(origin, vectors) {
            return Vector2.getAverageVector(vectors).subtract(origin);
        }
    }, {
        key: 'getNearestVector',
        value: function getNearestVector(origin, vectors) {
            return vectors.map(function (v) {
                return v.subtract(origin);
            }).sort(function (a, b) {
                return a.getSqrMagnitude() - b.getSqrMagnitude();
            })[0];
        }
    }, {
        key: 'forward',
        value: function forward() {
            return new Vector2(0, 0);
        }
    }, {
        key: 'back',
        value: function back() {
            return new Vector2(0, 0);
        }
    }, {
        key: 'left',
        value: function left() {
            return new Vector2(-1, 0);
        }
    }, {
        key: 'right',
        value: function right() {
            return new Vector2(1, 0);
        }
    }, {
        key: 'up',
        value: function up() {
            return new Vector2(0, 1);
        }
    }, {
        key: 'down',
        value: function down() {
            return new Vector2(0, -1);
        }
    }, {
        key: 'one',
        value: function one() {
            return new Vector2(1, 1);
        }
    }, {
        key: 'zero',
        value: function zero() {
            return new Vector2(0, 0);
        }
    }, {
        key: 'infinity',
        value: function infinity() {
            return new Vector2(Infinity, Infinity);
        }
    }, {
        key: 'negativeInfinity',
        value: function negativeInfinity() {
            return new Vector2(-Infinity, -Infinity);
        }
    }]);

    return Vector2;
}();

/* harmony default export */ exports["a"] = Vector2;

/***/ },
/* 4 */
/***/ function(module, exports) {

// stats.js - http://github.com/mrdoob/stats.js
var Stats=function(){function h(a){c.appendChild(a.dom);return a}function k(a){for(var d=0;d<c.children.length;d++)c.children[d].style.display=d===a?"block":"none";l=a}var l=0,c=document.createElement("div");c.style.cssText="position:fixed;top:0;left:0;cursor:pointer;opacity:0.9;z-index:10000";c.addEventListener("click",function(a){a.preventDefault();k(++l%c.children.length)},!1);var g=(performance||Date).now(),e=g,a=0,r=h(new Stats.Panel("FPS","#0ff","#002")),f=h(new Stats.Panel("MS","#0f0","#020"));
if(self.performance&&self.performance.memory)var t=h(new Stats.Panel("MB","#f08","#201"));k(0);return{REVISION:16,dom:c,addPanel:h,showPanel:k,begin:function(){g=(performance||Date).now()},end:function(){a++;var c=(performance||Date).now();f.update(c-g,200);if(c>e+1E3&&(r.update(1E3*a/(c-e),100),e=c,a=0,t)){var d=performance.memory;t.update(d.usedJSHeapSize/1048576,d.jsHeapSizeLimit/1048576)}return c},update:function(){g=this.end()},domElement:c,setMode:k}};
Stats.Panel=function(h,k,l){var c=Infinity,g=0,e=Math.round,a=e(window.devicePixelRatio||1),r=80*a,f=48*a,t=3*a,u=2*a,d=3*a,m=15*a,n=74*a,p=30*a,q=document.createElement("canvas");q.width=r;q.height=f;q.style.cssText="width:80px;height:48px";var b=q.getContext("2d");b.font="bold "+9*a+"px Helvetica,Arial,sans-serif";b.textBaseline="top";b.fillStyle=l;b.fillRect(0,0,r,f);b.fillStyle=k;b.fillText(h,t,u);b.fillRect(d,m,n,p);b.fillStyle=l;b.globalAlpha=.9;b.fillRect(d,m,n,p);return{dom:q,update:function(f,
v){c=Math.min(c,f);g=Math.max(g,f);b.fillStyle=l;b.globalAlpha=1;b.fillRect(0,0,r,m);b.fillStyle=k;b.fillText(e(f)+" "+h+" ("+e(c)+"-"+e(g)+")",t,u);b.drawImage(q,d+a,m,n-a,p,d,m,n-a,p);b.fillRect(d+n-a,m,a,p);b.fillStyle=l;b.globalAlpha=.9;b.fillRect(d+n-a,m,a,e((1-f/v)*p))}}};"object"===typeof module&&(module.exports=Stats);


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Random = function () {
    function Random() {
        _classCallCheck(this, Random);
    }

    _createClass(Random, null, [{
        key: "between",
        value: function between() {
            var min = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
            var max = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 100;
            var floor = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

            if (floor) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            } else {
                return Math.random() * (max - min) + min;
            }
        }
    }, {
        key: "bool",
        value: function bool() {
            return Math.random() >= .5;
        }
    }, {
        key: "fromArray",
        value: function fromArray(array) {
            return array[Random.between(0, array.length - 1)];
        }
    }]);

    return Random;
}();

/* harmony default export */ exports["a"] = Random;

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

/* eslint-disable no-undefined */

var throttle = __webpack_require__(7);

/**
 * Debounce execution of a function. Debouncing, unlike throttling,
 * guarantees that a function is only executed a single time, either at the
 * very beginning of a series of calls, or at the very end.
 *
 * @param  {Number}   delay         A zero-or-greater delay in milliseconds. For event callbacks, values around 100 or 250 (or even higher) are most useful.
 * @param  {Boolean}  atBegin       Optional, defaults to false. If atBegin is false or unspecified, callback will only be executed `delay` milliseconds
 *                                  after the last debounced-function call. If atBegin is true, callback will be executed only at the first debounced-function call.
 *                                  (After the throttled-function has not been called for `delay` milliseconds, the internal counter is reset).
 * @param  {Function} callback      A function to be executed after delay milliseconds. The `this` context and all arguments are passed through, as-is,
 *                                  to `callback` when the debounced-function is executed.
 *
 * @return {Function} A new, debounced function.
 */
module.exports = function ( delay, atBegin, callback ) {
	return callback === undefined ? throttle(delay, atBegin, false) : throttle(delay, callback, atBegin !== false);
};


/***/ },
/* 7 */
/***/ function(module, exports) {

/* eslint-disable no-undefined,no-param-reassign,no-shadow */

/**
 * Throttle execution of a function. Especially useful for rate limiting
 * execution of handlers on events like resize and scroll.
 *
 * @param  {Number}    delay          A zero-or-greater delay in milliseconds. For event callbacks, values around 100 or 250 (or even higher) are most useful.
 * @param  {Boolean}   noTrailing     Optional, defaults to false. If noTrailing is true, callback will only execute every `delay` milliseconds while the
 *                                    throttled-function is being called. If noTrailing is false or unspecified, callback will be executed one final time
 *                                    after the last throttled-function call. (After the throttled-function has not been called for `delay` milliseconds,
 *                                    the internal counter is reset)
 * @param  {Function}  callback       A function to be executed after delay milliseconds. The `this` context and all arguments are passed through, as-is,
 *                                    to `callback` when the throttled-function is executed.
 * @param  {Boolean}   debounceMode   If `debounceMode` is true (at begin), schedule `clear` to execute after `delay` ms. If `debounceMode` is false (at end),
 *                                    schedule `callback` to execute after `delay` ms.
 *
 * @return {Function}  A new, throttled, function.
 */
module.exports = function ( delay, noTrailing, callback, debounceMode ) {

	// After wrapper has stopped being called, this timeout ensures that
	// `callback` is executed at the proper times in `throttle` and `end`
	// debounce modes.
	var timeoutID;

	// Keep track of the last time `callback` was executed.
	var lastExec = 0;

	// `noTrailing` defaults to falsy.
	if ( typeof noTrailing !== 'boolean' ) {
		debounceMode = callback;
		callback = noTrailing;
		noTrailing = undefined;
	}

	// The `wrapper` function encapsulates all of the throttling / debouncing
	// functionality and when executed will limit the rate at which `callback`
	// is executed.
	function wrapper () {

		var self = this;
		var elapsed = Number(new Date()) - lastExec;
		var args = arguments;

		// Execute `callback` and update the `lastExec` timestamp.
		function exec () {
			lastExec = Number(new Date());
			callback.apply(self, args);
		}

		// If `debounceMode` is true (at begin) this is used to clear the flag
		// to allow future `callback` executions.
		function clear () {
			timeoutID = undefined;
		}

		if ( debounceMode && !timeoutID ) {
			// Since `wrapper` is being called for the first time and
			// `debounceMode` is true (at begin), execute `callback`.
			exec();
		}

		// Clear any existing timeout.
		if ( timeoutID ) {
			clearTimeout(timeoutID);
		}

		if ( debounceMode === undefined && elapsed > delay ) {
			// In throttle mode, if `delay` time has been exceeded, execute
			// `callback`.
			exec();

		} else if ( noTrailing !== true ) {
			// In trailing throttle mode, since `delay` time has not been
			// exceeded, schedule `callback` to execute `delay` ms after most
			// recent execution.
			//
			// If `debounceMode` is true (at begin), schedule `clear` to execute
			// after `delay` ms.
			//
			// If `debounceMode` is false (at end), schedule `callback` to
			// execute after `delay` ms.
			timeoutID = setTimeout(debounceMode ? clear : exec, debounceMode === undefined ? delay - elapsed : delay);
		}

	}

	// Return the wrapper function.
	return wrapper;

};


/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__lib_vector2__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__lib_random__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__lib_math_e__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__engine_physics__ = __webpack_require__(27);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }






var Entity = function () {
    _createClass(Entity, [{
        key: 'direction',
        set: function set(val) {
            this._direction = __WEBPACK_IMPORTED_MODULE_2__lib_math_e__["a" /* default */].wrapAngleRad(val);
        },
        get: function get() {
            return this._direction;
        }
    }]);

    function Entity(game) {
        var position = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : new __WEBPACK_IMPORTED_MODULE_0__lib_vector2__["a" /* default */]();
        var parent = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

        _classCallCheck(this, Entity);

        this.hasCollisions = false;
        this.physics = false;
        this._direction = 0;
        this.minDistanceBetweenEntities = 0;
        this.polygons = [];

        this.game = game;
        this.position = position;
        this.parent = parent;
    }

    _createClass(Entity, [{
        key: 'placeInOutsideField',
        value: function placeInOutsideField() {
            this.placeCentered(new __WEBPACK_IMPORTED_MODULE_0__lib_vector2__["a" /* default */](-1, -1));
        }
    }, {
        key: 'placeInRandomField',
        value: function placeInRandomField() {
            var emptyFields = this.game.scene.entities.filter(function (wall) {
                return wall.type === 0;
            });
            this.placeCentered(__WEBPACK_IMPORTED_MODULE_1__lib_random__["a" /* default */].fromArray(emptyFields).position);
        }
    }, {
        key: 'placeInCenterField',
        value: function placeInCenterField() {
            this.placeCentered(new __WEBPACK_IMPORTED_MODULE_0__lib_vector2__["a" /* default */](0, 0));
        }
    }, {
        key: 'placeCentered',
        value: function placeCentered(position) {
            this.position = position.floor().add(.5, .5);
        }

        /**
         * TODO: normalize multiple vectors to prevent faster diagonal moving
         */

    }, {
        key: 'move',
        value: function move(distance, direction) {
            var newPosition = this.position.add(__WEBPACK_IMPORTED_MODULE_0__lib_vector2__["a" /* default */].fromPolar(distance, direction));

            if (!this.hasCollisions) {
                this.position = newPosition;
                return;
            }

            var wallAtPositionX = this.game.scene.getEntityAt(new __WEBPACK_IMPORTED_MODULE_0__lib_vector2__["a" /* default */](newPosition.x, this.position.y));
            var wallAtPositionY = this.game.scene.getEntityAt(new __WEBPACK_IMPORTED_MODULE_0__lib_vector2__["a" /* default */](this.position.x, newPosition.y));

            if (!wallAtPositionX) {
                this.position.x = newPosition.x;
            }

            if (!wallAtPositionY) {
                this.position.y = newPosition.y;
            }
        }
    }, {
        key: 'update',
        value: function update(delta) {
            if (this.physics) {
                __WEBPACK_IMPORTED_MODULE_3__engine_physics__["a" /* default */].transformEntity(this);
            }
        }
    }]);

    return Entity;
}();

/* harmony default export */ exports["a"] = Entity;

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__math_e__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__string_e__ = __webpack_require__(33);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }




var Color = function () {
    _createClass(Color, [{
        key: 'r',
        set: function set(val) {
            this._r = __WEBPACK_IMPORTED_MODULE_0__math_e__["a" /* default */].clamp(Math.round(val), 0, 255);
        },
        get: function get() {
            return this._r;
        }
    }, {
        key: 'g',
        set: function set(val) {
            this._g = __WEBPACK_IMPORTED_MODULE_0__math_e__["a" /* default */].clamp(Math.round(val), 0, 255);
        },
        get: function get() {
            return this._g;
        }
    }, {
        key: 'b',
        set: function set(val) {
            this._b = __WEBPACK_IMPORTED_MODULE_0__math_e__["a" /* default */].clamp(Math.round(val), 0, 255);
        },
        get: function get() {
            return this._b;
        }
    }, {
        key: 'a',
        set: function set(val) {
            this._a = __WEBPACK_IMPORTED_MODULE_0__math_e__["a" /* default */].clamp(val, 0, 1);
        },
        get: function get() {
            return this._a;
        }
    }]);

    function Color() {
        var r = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var g = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
        var b = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
        var a = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 1;

        _classCallCheck(this, Color);

        this._r = 0;
        this._g = 0;
        this._b = 0;
        this._a = 1;

        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    _createClass(Color, [{
        key: 'toHex',
        value: function toHex() {
            return '#' + Color.formatAsHex(this.r) + Color.formatAsHex(this.g) + Color.formatAsHex(this.b) + (this.a !== 1 ? Color.formatAsHex(this.a * 255) : '');
        }
    }, {
        key: 'toRGBA',
        value: function toRGBA() {
            return 'rgba(' + this.r + ', ' + this.g + ', ' + this.b + ', ' + this.a + ')';
        }
    }, {
        key: 'equals',
        value: function equals(color) {
            return this.r === color.r && this.g === color.g && this.b === color.b && this.a === color.a;
        }
    }, {
        key: Symbol.toStringTag,
        get: function get() {
            return this.toRGBA();
        }
    }], [{
        key: 'formatAsHex',
        value: function formatAsHex(val) {
            return __WEBPACK_IMPORTED_MODULE_1__string_e__["a" /* default */].leftPad(Math.round(val).toString(16), 2);
        }
    }]);

    return Color;
}();

/* harmony default export */ exports["a"] = Color;

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_stats_js__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_stats_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_stats_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__lib_draw__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__controls__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__scene__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__entity_player__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__camera_default__ = __webpack_require__(25);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }











var Game = function () {
    function Game(canvas) {
        _classCallCheck(this, Game);

        this.isRunning = false;
        this.canvasWidth = window.innerWidth;
        this.canvasHeight = window.innerHeight;
        this.lastTime = 0;

        this.canvas = canvas;
        this.ctx = this.canvas.getContext('2d');

        this.camera = new __WEBPACK_IMPORTED_MODULE_5__camera_default__["a" /* default */](this);
        this.player = new __WEBPACK_IMPORTED_MODULE_4__entity_player__["a" /* default */](this);
        this.scene = new __WEBPACK_IMPORTED_MODULE_3__scene__["a" /* default */](this);
        this.controls = new __WEBPACK_IMPORTED_MODULE_2__controls__["a" /* default */](this);
        this.draw = new __WEBPACK_IMPORTED_MODULE_1__lib_draw__["a" /* default */](this);

        this.stats = new __WEBPACK_IMPORTED_MODULE_0_stats_js___default.a();
        document.body.appendChild(this.stats.dom);

        this.initCanvas();
        this.scene.buildRandom(16);
        this.player.placeInOutsideField();
    }

    _createClass(Game, [{
        key: 'initCanvas',
        value: function initCanvas() {
            var _this = this;

            this.canvas.width = this.canvasWidth;
            this.canvas.height = this.canvasHeight;
            this.canvas.onclick = function () {
                return _this.canvas.requestPointerLock();
            };
        }
    }, {
        key: 'start',
        value: function start() {
            this.isRunning = true;
            this.lastTime = 0;
            this.stats.showPanel(0);
            this.loop();
        }
    }, {
        key: 'stop',
        value: function stop() {
            this.isRunning = false;
        }
    }, {
        key: 'loop',
        value: function loop(time) {
            if (!this.isRunning) {
                return;
            }

            this.stats.begin();

            var deltaSeconds = 0;
            if (time && this.lastTime) {
                deltaSeconds = (time - this.lastTime) / 1000;
            }
            this.lastTime = time;

            this.scene.update(deltaSeconds);
            this.player.update(deltaSeconds);
            this.camera.draw();

            this.stats.end();
            window.requestAnimationFrame(this.loop.bind(this));
        }
    }]);

    return Game;
}();

/* harmony default export */ exports["a"] = Game;

/***/ },
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CameraBase = function () {
    function CameraBase(game) {
        _classCallCheck(this, CameraBase);

        this.game = game;
    }

    _createClass(CameraBase, [{
        key: "draw",
        value: function draw() {
            throw new Error("draw not implemented by camera");
        }
    }, {
        key: "position",
        get: function get() {
            return this.game.player.position;
        }
    }, {
        key: "direction",
        get: function get() {
            return this.game.player.direction;
        }
    }]);

    return CameraBase;
}();

/* harmony default export */ exports["a"] = CameraBase;

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__lib_math_e__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__lib_vector2__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__lib_color__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__camera_base__ = __webpack_require__(24);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }







var CameraDefault = function (_CameraBase) {
    _inherits(CameraDefault, _CameraBase);

    function CameraDefault(game) {
        _classCallCheck(this, CameraDefault);

        var _this = _possibleConstructorReturn(this, (CameraDefault.__proto__ || Object.getPrototypeOf(CameraDefault)).call(this, game));

        _this.focalHeight = 1;
        _this.viewAngle = __WEBPACK_IMPORTED_MODULE_0__lib_math_e__["a" /* default */].degToRad(100);
        _this.maxViewDistance = 15;
        _this.minViewDistance = 0;
        _this.eyeHeight = .4;
        return _this;
    }

    _createClass(CameraDefault, [{
        key: 'draw',
        value: function draw() {
            this.game.draw.clear();
            this.drawSky();
            this.drawFloor();
            this.drawScene();
            this.drawPlayer();
            //this.drawOverview();
        }
    }, {
        key: 'drawSky',
        value: function drawSky() {
            this.game.draw.angledImage(this.game.scene.skyRes, new __WEBPACK_IMPORTED_MODULE_1__lib_vector2__["a" /* default */](), this.game.canvasWidth, this.game.canvasHeight, this.direction, this.viewAngle);
        }
    }, {
        key: 'drawFloor',
        value: function drawFloor() {
            this.game.draw.linearGradient(new __WEBPACK_IMPORTED_MODULE_1__lib_vector2__["a" /* default */](0, this.game.canvasHeight / 2), this.game.canvasWidth, this.game.canvasHeight / 2, [new __WEBPACK_IMPORTED_MODULE_2__lib_color__["a" /* default */](0, 0, 0), { offset: .1, color: new __WEBPACK_IMPORTED_MODULE_2__lib_color__["a" /* default */](0, 0, 0) }, { offset: .4, color: new __WEBPACK_IMPORTED_MODULE_2__lib_color__["a" /* default */](80, 80, 80) }, new __WEBPACK_IMPORTED_MODULE_2__lib_color__["a" /* default */](180, 180, 180)]);
        }
    }, {
        key: 'drawScene',
        value: function drawScene() {
            var _this2 = this;

            var polygons = Array.prototype.concat.apply([], this.game.scene.entities.filter(function (entity) {
                return _this2.isVectorInViewDistance(entity.position) && _this2.isVectorInViewField(entity.points);
            }).map(function (entity) {
                return entity.polygons;
            }));

            polygons.filter(function (polygon) {
                return _this2.isPolygonFaceVisible(polygon);
            }).map(function (polygon) {
                return _this2.projectPolygon(polygon);
            }).sort(function (a, b) {
                return b.distance - a.distance;
            }).forEach(function (plane) {
                return _this2.drawPlane(plane);
            });
        }
    }, {
        key: 'isPolygonFaceVisible',
        value: function isPolygonFaceVisible(polygon) {
            return __WEBPACK_IMPORTED_MODULE_0__lib_math_e__["a" /* default */].isAngleWithin(this.position.angleToVector(polygon.origin), polygon.normal - Math.PI * .5, polygon.normal + Math.PI * .5);
        }
    }, {
        key: 'shouldInvertAngle',
        value: function shouldInvertAngle(polygon, point, angleToPlayer) {
            var _this3 = this;

            var isPointInView = Math.abs(angleToPlayer) < this.viewAngle / 2;
            if (isPointInView) {
                // early exit
                return false;
            }

            var angleToPoints = polygon.points.map(function (point) {
                return _this3.position.angleToVector(point);
            });
            var anglePointA = __WEBPACK_IMPORTED_MODULE_0__lib_math_e__["a" /* default */].wrapAngleRad(angleToPoints[0] + Math.PI);
            var anglePointB = __WEBPACK_IMPORTED_MODULE_0__lib_math_e__["a" /* default */].wrapAngleRad(angleToPoints[1] + Math.PI);
            var inverseViewAngle = __WEBPACK_IMPORTED_MODULE_0__lib_math_e__["a" /* default */].wrapAngleRad(this.direction + Math.PI);

            return __WEBPACK_IMPORTED_MODULE_0__lib_math_e__["a" /* default */].isAngleWithin(inverseViewAngle, anglePointB, anglePointA);
        }
    }, {
        key: 'projectPolygon',
        value: function projectPolygon(polygon) {
            var _this4 = this;

            var projectedPoints = [];
            var accumulatedDistance = 0;

            polygon.points.forEach(function (point, i) {
                // position of the point relative to the camera
                var pointRelative = point.subtract(_this4.position);

                // angle relative to the cameras direction (-0.5 viewport to 0.5 viewport)
                var angleToPlayer = __WEBPACK_IMPORTED_MODULE_0__lib_math_e__["a" /* default */].angleBetweenAngles(_this4.direction, pointRelative.toPolar());

                if (_this4.shouldInvertAngle(polygon, point, angleToPlayer)) {
                    angleToPlayer = -angleToPlayer;
                }

                // correct distance for fisheye effect
                var pointMagnitude = pointRelative.getMagnitude();
                var pointDistance = pointMagnitude / Math.cosh(angleToPlayer);

                // height and x/y-position of the line on screen
                var drawHeight = _this4.game.canvasHeight / pointDistance * _this4.focalHeight * polygon.drawHeight;
                var drawY = _this4.game.canvasHeight / 2 - drawHeight * _this4.eyeHeight;
                var drawX = (_this4.viewAngle / 2 + angleToPlayer) / _this4.viewAngle * _this4.game.canvasWidth;

                accumulatedDistance += pointMagnitude;

                projectedPoints.push({
                    top: new __WEBPACK_IMPORTED_MODULE_1__lib_vector2__["a" /* default */](drawX, drawY),
                    bottom: new __WEBPACK_IMPORTED_MODULE_1__lib_vector2__["a" /* default */](drawX, drawY + drawHeight)
                });
            });

            return {
                points: [projectedPoints[0].top, projectedPoints[0].bottom, projectedPoints[1].bottom, projectedPoints[1].top],
                distance: accumulatedDistance / polygon.points.length
            };
        }
    }, {
        key: 'drawPlane',
        value: function drawPlane(plane) {
            var shade = __WEBPACK_IMPORTED_MODULE_0__lib_math_e__["a" /* default */].lerp(200, 0, plane.distance / this.maxViewDistance);
            this.game.draw.polygon(new __WEBPACK_IMPORTED_MODULE_2__lib_color__["a" /* default */](shade, shade, shade), plane.points);
        }
    }, {
        key: 'drawPlayer',
        value: function drawPlayer() {}
    }, {
        key: 'drawOverview',
        value: function drawOverview() {
            var _this5 = this;

            var scale = 20;
            var leftBase = this.game.canvasWidth / 2;
            var bottomBase = this.game.canvasHeight / 2;

            this.game.ctx.save();
            this.game.ctx.translate(leftBase, bottomBase);
            this.game.ctx.scale(1, -1);

            this.game.scene.entities.forEach(function (wall) {
                _this5.game.draw.rect(_this5.isVectorInViewDistance(wall.position) && _this5.isVectorInViewField(wall.points) ? '#000' : '#777', wall.position.multiply(scale), scale, scale);
            });

            this.game.draw.line('#333', this.game.player.position.multiply(scale), this.game.player.position.add(__WEBPACK_IMPORTED_MODULE_1__lib_vector2__["a" /* default */].fromPolar(this.maxViewDistance, this.direction)).multiply(scale));
            this.game.draw.line('#999', this.game.player.position.subtract(__WEBPACK_IMPORTED_MODULE_1__lib_vector2__["a" /* default */].fromPolar(this.maxViewDistance, this.direction)).multiply(scale), this.game.player.position.multiply(scale), [5]);
            this.game.draw.line('#eee', this.game.player.position.add(__WEBPACK_IMPORTED_MODULE_1__lib_vector2__["a" /* default */].fromPolar(this.maxViewDistance, this.direction - this.viewAngle / 2)).multiply(scale), this.game.player.position.multiply(scale), [5]);
            this.game.draw.line('#eee', this.game.player.position.add(__WEBPACK_IMPORTED_MODULE_1__lib_vector2__["a" /* default */].fromPolar(this.maxViewDistance, this.direction + this.viewAngle / 2)).multiply(scale), this.game.player.position.multiply(scale), [5]);

            this.game.draw.rect('#ffe600', this.game.player.position.multiply(scale));

            this.game.ctx.restore();
        }
    }, {
        key: 'isVectorInViewDistance',
        value: function isVectorInViewDistance(worldPosition) {
            var relativeDistance = worldPosition.subtract(this.position).getMagnitude();
            return relativeDistance < this.maxViewDistance && relativeDistance > this.minViewDistance;
        }
    }, {
        key: 'isVectorInViewField',
        value: function isVectorInViewField(worldPosition) {
            var _this6 = this;

            if (Array.isArray(worldPosition)) {
                return worldPosition.some(function (pos) {
                    return _this6.isVectorInViewField(pos);
                });
            }

            var relativePosition = worldPosition.subtract(this.position);
            var angleToXAxis = relativePosition.toPolar();

            return __WEBPACK_IMPORTED_MODULE_0__lib_math_e__["a" /* default */].isAngleWithin(angleToXAxis, this.direction - this.viewAngle / 2, this.direction + this.viewAngle / 2);
        }
    }]);

    return CameraDefault;
}(__WEBPACK_IMPORTED_MODULE_3__camera_base__["a" /* default */]);

/* harmony default export */ exports["a"] = CameraDefault;

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_throttle_debounce_debounce__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_throttle_debounce_debounce___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_throttle_debounce_debounce__);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Controls = function () {
    function Controls(game) {
        _classCallCheck(this, Controls);

        this.kbdValues = {
            37: 'left',
            39: 'right',
            38: 'forward',
            40: 'backward',
            65: 'left',
            68: 'right',
            83: 'backward',
            87: 'forward'
        };
        this.kbdToggles = {
            16: 'isSprinting'
        };
        this.states = {
            left: 0,
            right: 0,
            forward: 0,
            backward: 0,
            rotation: 0,
            isSprinting: false
        };

        this.game = game;
        this.registerEvents();
    }

    _createClass(Controls, [{
        key: 'registerEvents',
        value: function registerEvents() {
            document.addEventListener('keydown', this.onKey.bind(this, true), false);
            document.addEventListener('keyup', this.onKey.bind(this, false), false);
            document.addEventListener('mousemove', this.onMouseMove.bind(this), false);
            document.addEventListener('mousemove', __WEBPACK_IMPORTED_MODULE_0_throttle_debounce_debounce___default()(100, this.onMouseMoveEnd.bind(this)), false);
        }
    }, {
        key: 'onKey',
        value: function onKey(isPressed, e) {
            var stateValueId = this.kbdValues[e.keyCode];
            var stateToggleId = this.kbdToggles[e.keyCode];

            if (stateValueId) {
                this.states[stateValueId] = isPressed ? 1 : 0;
            }

            if (stateToggleId) {
                this.states[stateToggleId] = isPressed;
            }

            if (stateValueId || stateToggleId) {
                e.preventDefault();
                e.stopPropagation();
            }
        }
    }, {
        key: 'onMouseMove',
        value: function onMouseMove(e) {
            this.states.rotation = e.movementX / window.innerWidth * -100;
        }
    }, {
        key: 'onMouseMoveEnd',
        value: function onMouseMoveEnd() {
            this.states.rotation = 0;
        }
    }]);

    return Controls;
}();

/* harmony default export */ exports["a"] = Controls;

/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Physics = function () {
    function Physics() {
        _classCallCheck(this, Physics);
    }

    _createClass(Physics, null, [{
        key: "transformEntity",
        value: function transformEntity(entity) {
            console.log(entity.physicProperties);
        }
    }]);

    return Physics;
}();

/* harmony default export */ exports["a"] = Physics;

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return ImageRessource; });
/* unused harmony export BinaryRessource */
var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var resCache = new Map();

var Ressource = function () {
    function Ressource(id, url) {
        _classCallCheck(this, Ressource);

        this.TYPE = 'Unknown';
        this.hasLoaded = false;
        this.isLoading = false;
        this.isSuccess = null;
        this.data = null;
        this.meta = {};

        this.id = id;
        this.url = url;

        resCache.set(id, this);
        this.load();
    }

    _createClass(Ressource, [{
        key: 'load',
        value: function load() {
            var _this = this;

            this.isLoading = true;
            this.meta.loadingTimeStart = Date.now();

            return function (isSuccess) {
                if (_this.hasLoaded) {
                    return false;
                }

                _this.isSuccess = isSuccess;
                _this.hasLoaded = true;
                _this.isLoading = false;
                _this.meta.loadingTimeEnd = Date.now();
                _this.meta.loadingDuration = _this.meta.loadingTimeEnd - _this.meta.loadingTimeStart;

                if (!isSuccess) {
                    console.warn('couldn\'t load ressource ' + _this.id);
                    return false;
                }

                return true;
            };
        }
    }]);

    return Ressource;
}();

/* unused harmony default export */ var _unused_webpack_default_export = Ressource;


var ImageRessource = function (_Ressource) {
    _inherits(ImageRessource, _Ressource);

    function ImageRessource() {
        var _ref;

        var _temp, _this2, _ret;

        _classCallCheck(this, ImageRessource);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this2 = _possibleConstructorReturn(this, (_ref = ImageRessource.__proto__ || Object.getPrototypeOf(ImageRessource)).call.apply(_ref, [this].concat(args))), _this2), _this2.TYPE = 'Image', _temp), _possibleConstructorReturn(_this2, _ret);
    }

    _createClass(ImageRessource, [{
        key: 'load',
        value: function load() {
            var _this3 = this;

            var img = new Image();
            var superHandler = _get(ImageRessource.prototype.__proto__ || Object.getPrototypeOf(ImageRessource.prototype), 'load', this).call(this);
            var handler = function handler(isSuccess) {
                if (!superHandler(isSuccess)) {
                    return;
                }

                _this3.data = img;
                _this3.meta.width = img.width;
                _this3.meta.height = img.height;
            };

            img.onload = handler.bind(null, true);
            img.onerror = handler.bind(null, false);
            img.onabort = handler.bind(null, false);
            img.oncancel = handler.bind(null, false);
            img.src = this.url;
        }
    }]);

    return ImageRessource;
}(Ressource);

var BinaryRessource = function (_Ressource2) {
    _inherits(BinaryRessource, _Ressource2);

    function BinaryRessource() {
        var _ref2;

        var _temp2, _this4, _ret2;

        _classCallCheck(this, BinaryRessource);

        for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
            args[_key2] = arguments[_key2];
        }

        return _ret2 = (_temp2 = (_this4 = _possibleConstructorReturn(this, (_ref2 = BinaryRessource.__proto__ || Object.getPrototypeOf(BinaryRessource)).call.apply(_ref2, [this].concat(args))), _this4), _this4.TYPE = 'Binary', _temp2), _possibleConstructorReturn(_this4, _ret2);
    }

    _createClass(BinaryRessource, [{
        key: 'load',
        value: function load() {
            var _this5 = this;

            var xhr = new XMLHttpRequest();
            var superHandler = _get(BinaryRessource.prototype.__proto__ || Object.getPrototypeOf(BinaryRessource.prototype), 'load', this).call(this);
            var handler = function handler(isSuccess) {
                isSuccess = isSuccess && xhr.status === 200;

                if (!superHandler(isSuccess)) {
                    return;
                }

                _this5.data = xhr.response;
            };

            xhr.onloadend = handler.bind(null, true);
            xhr.onerror = handler.bind(null, false);
            xhr.onabort = handler.bind(null, false);
            xhr.ontimeout = handler.bind(null, false);

            xhr.open(this.url, 'GET');
            xhr.send();
        }
    }]);

    return BinaryRessource;
}(Ressource);

/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__lib_vector2__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__lib_random__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ressource__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__entity_wall__ = __webpack_require__(31);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }






var Scene = function () {
    function Scene(game) {
        _classCallCheck(this, Scene);

        this.entities = [];
        this.skyRes = new __WEBPACK_IMPORTED_MODULE_2__ressource__["a" /* ImageRessource */]('map-sky', 'https://raw.githubusercontent.com/hunterloftis/playfuljs-demos/gh-pages/raycaster/assets/deathvalley_panorama.jpg');

        this.game = game;
    }

    _createClass(Scene, [{
        key: 'iteratePositions',
        value: function iteratePositions(width) {
            var height = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : width;
            var iterator = arguments[2];

            for (var x = 0; x < width; x++) {
                for (var y = 0; y < height; y++) {
                    iterator(x, y);
                }
            }
        }
    }, {
        key: 'buildRandom',
        value: function buildRandom(width) {
            var _this = this;

            var height = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : width;

            this.iteratePositions(width, height, function (x, y) {
                if (Math.random() > 0.7) {
                    _this.entities.push(new __WEBPACK_IMPORTED_MODULE_3__entity_wall__["a" /* default */](_this.game, new __WEBPACK_IMPORTED_MODULE_0__lib_vector2__["a" /* default */](x, y)));
                }
            });
        }
    }, {
        key: 'buildFill',
        value: function buildFill(width) {
            var _this2 = this;

            var height = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : width;

            this.iteratePositions(width, height, function (x, y) {
                _this2.entities.push(new __WEBPACK_IMPORTED_MODULE_3__entity_wall__["a" /* default */](_this2.game, new __WEBPACK_IMPORTED_MODULE_0__lib_vector2__["a" /* default */](x, y)));
            });
        }
    }, {
        key: 'buildBasic',
        value: function buildBasic() {
            this.buildWalls(10, 5);
        }
    }, {
        key: 'buildWalls',
        value: function buildWalls(width) {
            var _this3 = this;

            var height = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : width;

            this.iteratePositions(width, height, function (x, y) {
                if (x === 0 || x === width - 1 || y === 0 || y === height - 1) {
                    _this3.entities.push(new __WEBPACK_IMPORTED_MODULE_3__entity_wall__["a" /* default */](_this3.game, new __WEBPACK_IMPORTED_MODULE_0__lib_vector2__["a" /* default */](x, y)));
                }
            });
        }
    }, {
        key: 'getEntityAt',
        value: function getEntityAt(position) {
            return this.entities.find(function (wall) {
                return wall.position.floor().equals(position.floor());
            });
        }
    }, {
        key: 'update',
        value: function update() {}
    }]);

    return Scene;
}();

/* harmony default export */ exports["a"] = Scene;

/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__lib_vector2__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__lib_random__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__lib_math_e__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__entity__ = __webpack_require__(8);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var Player = function (_Entity) {
    _inherits(Player, _Entity);

    function Player(game) {
        _classCallCheck(this, Player);

        var _this = _possibleConstructorReturn(this, (Player.__proto__ || Object.getPrototypeOf(Player)).call(this, game));

        _this.moveSpeedForwards = 3;
        _this.moveSpeedSidewards = 2;
        _this.moveSpeedSprinting = 5;
        _this.sprintingRemainingDuration = 0;
        _this.sprintingAvailableDuration = 4;
        _this.sprintingRefillFactor = .5;
        _this.hasCollisions = true;

        _this.sprintingRemainingDuration = _this.sprintingAvailableDuration;
        return _this;
    }

    _createClass(Player, [{
        key: 'update',
        value: function update(delta) {
            _get(Player.prototype.__proto__ || Object.getPrototypeOf(Player.prototype), 'update', this).call(this, delta);

            if (this.game.controls.states.left !== 0) {
                this.move(this.moveSpeedSidewards * this.game.controls.states.left * delta, this.direction + 0.5 * Math.PI);
            }

            if (this.game.controls.states.right !== 0) {
                this.move(this.moveSpeedSidewards * this.game.controls.states.right * delta, this.direction + 1.5 * Math.PI);
            }

            if (this.game.controls.states.forward !== 0) {
                var speed = this.moveSpeedForwards * this.game.controls.states.forward * delta;

                if (this.game.controls.states.isSprinting && this.sprintingRemainingDuration > 0) {
                    speed = this.moveSpeedSprinting * this.game.controls.states.forward * delta;
                    this.sprintingRemainingDuration -= delta;
                }

                this.move(speed, this.direction);
            }

            if (this.game.controls.states.backward !== 0) {
                this.move(-this.moveSpeedForwards * this.game.controls.states.backward * delta, this.direction);
            }

            if (this.game.controls.states.rotation !== 0) {
                this.direction += Math.PI * this.game.controls.states.rotation * delta;
            }

            if (!this.game.controls.states.isSprinting && this.sprintingRemainingDuration <= this.sprintingAvailableDuration) {
                this.sprintingRemainingDuration += delta * this.sprintingRefillFactor;
            }
        }
    }]);

    return Player;
}(__WEBPACK_IMPORTED_MODULE_3__entity__["a" /* default */]);

/* harmony default export */ exports["a"] = Player;

/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__entity__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__lib_vector2__ = __webpack_require__(3);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var Wall = function (_Entity) {
    _inherits(Wall, _Entity);

    function Wall(position, game) {
        _classCallCheck(this, Wall);

        var _this = _possibleConstructorReturn(this, (Wall.__proto__ || Object.getPrototypeOf(Wall)).call(this, position, game));

        _this.hasCollisions = true;
        _this.drawHeight = 1;

        _this.setData();
        return _this;
    }

    _createClass(Wall, [{
        key: 'update',
        value: function update(delta) {
            _get(Wall.prototype.__proto__ || Object.getPrototypeOf(Wall.prototype), 'update', this).call(this, delta);
        }
    }, {
        key: 'setData',
        value: function setData() {
            this.points = [this.position, this.position.add(1, 0), this.position.add(1, 1), this.position.add(0, 1)];

            this.polygons = [{ drawHeight: this.drawHeight, points: [this.points[0], this.points[1]], normal: Math.PI * 1.5, origin: this.points[0].lerp(this.points[1], .5) }, { drawHeight: this.drawHeight, points: [this.points[1], this.points[2]], normal: 0, origin: this.points[1].lerp(this.points[2], .5) }, { drawHeight: this.drawHeight, points: [this.points[2], this.points[3]], normal: Math.PI * 0.5, origin: this.points[2].lerp(this.points[3], .5) }, { drawHeight: this.drawHeight, points: [this.points[3], this.points[0]], normal: Math.PI, origin: this.points[3].lerp(this.points[0], .5) }];
        }
    }, {
        key: 'getPolygons',
        value: function getPolygons() {}
    }]);

    return Wall;
}(__WEBPACK_IMPORTED_MODULE_0__entity__["a" /* default */]);

/* harmony default export */ exports["a"] = Wall;

/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__math_e__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__color__ = __webpack_require__(9);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }




var Draw = function () {
    function Draw(game) {
        _classCallCheck(this, Draw);

        this.game = game;
        this.ctx = this.game.ctx;
    }

    _createClass(Draw, [{
        key: 'clear',
        value: function clear() {
            this.ctx.clearRect(0, 0, this.game.canvasWidth, this.game.canvasHeight);
        }
    }, {
        key: 'line',
        value: function line(color, a, b, lineDash) {
            this.ctx.save();
            if (lineDash) {
                this.ctx.setLineDash(lineDash);
            }
            this.ctx.strokeStyle = color;
            this.ctx.beginPath();
            this.ctx.moveTo(a.x, a.y);
            this.ctx.lineTo(b.x, b.y);
            this.ctx.stroke();
            this.ctx.restore();
        }
    }, {
        key: 'rect',
        value: function rect(color, position) {
            var width = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
            var height = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : width;

            this.ctx.save();
            this.ctx.fillStyle = color;
            this.ctx.fillRect(position.x, position.y, width, height);
            this.ctx.restore();
        }
    }, {
        key: 'polygon',
        value: function polygon(color, points) {
            var _this = this;

            this.ctx.save();
            if (!points || points.length === 0) {
                return;
            }

            this.ctx.beginPath();
            this.ctx.moveTo(points[0].x, points[0].y);

            points.slice(1).forEach(function (point) {
                _this.ctx.lineTo(point.x, point.y);
            });

            this.ctx.fillStyle = color.toRGBA();

            this.ctx.fill();
            this.ctx.restore();
        }
    }, {
        key: 'linearGradient',
        value: function linearGradient(pos, w, h, stops) {
            var gradient = this.ctx.createLinearGradient(pos.x, pos.y, pos.x, pos.y + h);
            stops.forEach(function (s, i) {
                if (s instanceof __WEBPACK_IMPORTED_MODULE_1__color__["a" /* default */]) {
                    gradient.addColorStop(i / (stops.length - 1), s.toRGBA());
                } else {
                    gradient.addColorStop(s.offset, s.color.toRGBA());
                }
            });

            this.ctx.save();
            this.ctx.fillStyle = gradient;
            this.ctx.fillRect(pos.x, pos.y, w, h);
            this.ctx.restore();
        }
    }, {
        key: 'radialGradient',
        value: function radialGradient(pos0, pos1, pos2, r1, r2, w, h, stops) {
            var gradient = this.ctx.createRadialGradient(pos1.x, pos1.y, r1, pos2.x, pos2.y, r2);

            stops.forEach(function (s, i) {
                gradient.addColorStop(s.offset, s.color.toRGBA());
            });

            this.ctx.save();
            this.ctx.fillStyle = gradient;
            this.ctx.fillRect(pos0.x, pos0.y, w, h);
            this.ctx.restore();
        }
    }, {
        key: 'angledImage',
        value: function angledImage(imgRes, pos, w, h, angle, viewAngle) {
            if (!imgRes.isSuccess) {
                return;
            }

            this.ctx.save();

            var angleScreenFactor = viewAngle / __WEBPACK_IMPORTED_MODULE_0__math_e__["a" /* default */].PI2;
            var imageFactor = 1 / angleScreenFactor;
            var fullAngleWidth = imgRes.meta.width * imageFactor;
            var drawHeight = imgRes.meta.height * imageFactor;
            var percOfCircle = 1 - angle / __WEBPACK_IMPORTED_MODULE_0__math_e__["a" /* default */].PI2;
            var left = -1 * percOfCircle * fullAngleWidth;

            this.ctx.drawImage(imgRes.data, left, 0, fullAngleWidth, h);

            if (-1 * (left - this.game.canvasWidth) > fullAngleWidth) {
                this.ctx.drawImage(imgRes.data, left + fullAngleWidth, 0, fullAngleWidth, h);
            }

            this.ctx.restore();
        }
    }]);

    return Draw;
}();

/* harmony default export */ exports["a"] = Draw;

/***/ },
/* 33 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var StringE = function () {
    function StringE() {
        _classCallCheck(this, StringE);
    }

    _createClass(StringE, null, [{
        key: 'leftPad',
        value: function leftPad(str, len) {
            var padding = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '0';

            return len - str.length > 0 ? this.repeat(padding, len - str.length) : str;
        }
    }, {
        key: 'repeat',
        value: function repeat(str, times) {
            var ret = '';

            for (var i = 0; i < times; i++) {
                ret += str;
            }

            return ret;
        }
    }]);

    return StringE;
}();

/* harmony default export */ exports["a"] = StringE;

/***/ },
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */,
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__engine_game__ = __webpack_require__(10);


var game = new __WEBPACK_IMPORTED_MODULE_0__engine_game__["a" /* default */](document.getElementById('canvas'));
game.start();

/***/ }
/******/ ]);
//# sourceMappingURL=experiments.raytracer.js.map